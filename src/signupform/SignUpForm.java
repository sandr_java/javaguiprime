/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signupform;
import javafx.scene.control.RadioButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
/**
 *
 * @author sichu         
 */
public class SignUpForm {
    JFrame f;
    JLabel usernamelbl,passwordlbl,gender,hobbies,countrylbl;
    JTextField usernametxt;
    JPasswordField passwordtxt;
    JRadioButton malebutton,femalebutton;
    JCheckBox reading,visiting,playing;
    JComboBox country;
    SignUpForm(){
      f=new JFrame();
      f.setSize(500,500);
      f.setTitle("SignUpForm");
      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      f.setLayout(null); 
      
      usernamelbl =new JLabel("User Name: ");
      usernamelbl.setBounds(10, 20, 100, 25);
      f.add(usernamelbl);
      passwordlbl=new JLabel();
      passwordlbl.setBounds(10, 50, 100, 25);
      passwordlbl.setText("Password: ");
      f.add(passwordlbl);
     
      usernametxt= new JTextField();
      usernametxt.setBounds(110,20, 200, 25 );
      
      passwordtxt=new JPasswordField();
      passwordtxt.setBounds(110, 50, 200, 25);
      f.add(usernametxt);
      f.add(passwordtxt);
      
      gender=new JLabel("Gender: ");
      gender.setBounds(10,80,100,25);
      f.add(gender);
      
      //radio button
      malebutton=new JRadioButton("Male");
      malebutton.setBounds(115, 80, 80, 25);
      f.add(malebutton);
      f.setVisible(true); 
      
      femalebutton=new JRadioButton("Female");
      femalebutton.setBounds(200, 80, 80, 25);
      f.add(femalebutton);
      
      ButtonGroup bg= new ButtonGroup();//buttongroup is required so that only one
      //radio button is selected; By default multiple radio button can be selected
      //if no buttongroup is defined;
      bg.add(malebutton);
      bg.add(femalebutton);
      
//checkbox
      hobbies=new JLabel("Hobbies: ");
      hobbies.setBounds(10,110,80,25);
      f.add(hobbies);
      
      visiting =new JCheckBox("visiting");
      visiting.setBounds(115,110,80,25);
      f.add(visiting);
      
       
      reading =new JCheckBox("reading");
      reading.setBounds(200,110,80,25);
      f.add(reading);
      
      playing =new JCheckBox("playing");
      playing.setBounds(285,110,80,25);
      f.add(playing);
      
      countrylbl=new JLabel("Select Country");
      countrylbl.setBounds(10,140,120,25);
      f.add(countrylbl);
      String [] s={"Nepal","India","USA","China","Australia"}; 
      country= new JComboBox(s);
      country.setBounds(150,140,100,25);
      f.add(country);
     
      f.setVisible(true); 
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new SignUpForm();
    }
    
}
